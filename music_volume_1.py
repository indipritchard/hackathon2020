from initserver import *

inx = Input()

music = SfPlayer("monstersshort.wav", loop=True).out()


def control_music(src, music):
    avg = Average(Abs(src), size=44100)  # 44100 = 1s
    music.mul = 0.05 + Clip((avg - 0.02) * 3.5, 0, 0.4)


control_music(inx, music)

s.gui(locals())
