"""this synth is awful"""

from initserver import *


# SUCKS
def bright_synth_boi(root_freq=110, amp=1, disto_tuned_amt=0.25, disto_detuned_amount=0.1):
    root_saws = [
        SuperSaw(
            freq=root_freq,
            detune=detune,
            mul=amp * d_mul,
        )
        for detune, d_mul in [
            (0.1, 0.5),
            (0.3, 1),
            (0.5, 0.5),
            (0.7, 0.2),
        ]
    ]
    harms = [
        Harmonizer(saw, transpo=transpo)
        for transpo in [-12, -5, 7]  # power chord
        for saw in root_saws
    ]
    chorus_verbs = [
        Freeverb(
            Chorus(Mix(harms)),
            size=size,
            damp=0.7,
            bal=1,
            mul=s_mul,
        )
        for size, s_mul in [
            (0, 1),
            (0.1, 0.7),
            (0.6, 0.2),
            (0.8, 0.1),
        ]
    ]
    disto_tuned = [
        Disto(
            harm,
            drive=Sine(  # CRUNCHY lfo
                freq=3 + (4 * i/len(harms)),
                phase=(i/len(harms))
            ).range(0.1, 0.5),
            mul=disto_tuned_amt,
            slope=0.1,  # lotsa highs
        )
        for i, harm in enumerate(harms)
    ]
    disto_detuned = [
        Disto(
            Harmonizer(harm, transpo=-2),
            drive=Sine(  # wobble lfo
                freq=2 + (2 * i/len(harms)),
                phase=(i/len(harms))
            ).range(0.4, 0.8),
            mul=disto_detuned_amount,
            slope=0.8,  # lotsa highs
        )
        for i, harm in enumerate(harms)
    ]
    return Mix(chorus_verbs + disto_tuned + disto_detuned)


#beat = Metro(0.5).play()
#adsr_env = Adsr()
#beat_env = TrigEnv(beat, table=LinTable([(0, 0), (100, 1), (1000, 1), (3000, 0)]))

#my_synth = bright_synth_boi(root_freq=Sine(freq=0.1).range(50, 300), amp=beat_env).mix(2).out()

inx = Input()
my_synth = bright_synth_boi(root_freq=Yin(inx)/2).out()

s.gui(locals())
