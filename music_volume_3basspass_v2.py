from initserver import *

inx = Input()


FILEPATH = "looperman/looperman-l-0102056-0205857-full-strings-130.wav"  # no bass :(
FILEPATH = "looperman/looperman-l-0173301-0205231-estringsfast.wav"  # a lil bass
FILEPATH = "looperman/looperman-l-2267968-0205766-l-o-v-e-i-s-a-l-o-s-i-n-g-g-a-m-e.wav"  # eh
#FILEPATH = "looperman/looperman-l-2986535-0206157-cinematic-strings-staccato-section.wav"
FILEPATH = "looperman/looperman-l-0828980-0200029-sad-slidin-powerchords-g-50.wav"
music = SfPlayer(FILEPATH, loop=True)
music2 = Mix([Harmonizer(music, transpo=-24), Harmonizer(music, transpo=-12), music])


def control_music_multi(src, music):
    lowfreq = 130
    highfreq = 400
    music_low = ButLP(music, freq=lowfreq)
    music_mid = ButBP(
        music,
        freq=(lowfreq+highfreq)/2,
        q=(lowfreq+highfreq)/(2 * (highfreq-lowfreq))  # freq/bandwidth
    )
    music_high = ButHP(music, freq=highfreq)
    fol_fast = 6 * Follower2(
        src,
        risetime=0.3,
        falltime=3,
    )
    fol_slow = 12 * Follower2(
        src,
        risetime=4,
        falltime=8,
    )
    music_low.mul = Clip(0.05 + fol_fast + 2.5 * fol_slow, 0, 1.5)
    music_mid.mul = Clip(0.05 + fol_fast, 0, 0.55)
    music_high.mul = Clip(0.05 + fol_slow + fol_fast, 0, 0.65)
    return Mix([music_low, music_mid, music_high])


musics = control_music_multi(inx, music2).out()

s.gui(locals())
