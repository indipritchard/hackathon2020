"""this is just to show you should be careful if you want to
.stop() something so it stops .out()ing -- .stop() is not the
opposite of out, .stop() then .play() is! (is there a one-method
alternative...???)"""


from initserver import *

inx = Input()

music = SfPlayer("monstersshort.wav", loop=True)
musicharm = Harmonizer(music)

yay = False
def normal_or_harm():
    global yay
    yay = not yay
    if yay:
        music.stop()
        music.play()
        musicharm.out()
    else:
        musicharm.stop()
        music.out()


while True:
    time.sleep(3)
    normal_or_harm()


s.gui(locals())
