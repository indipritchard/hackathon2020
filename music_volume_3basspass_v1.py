from initserver import *

inx = Input()

music = SfPlayer("monstersshort.wav", loop=True)


def control_music_multi(src, music):
    lowfreq = 200
    highfreq = 500
    music_low = ButLP(music, freq=lowfreq)
    music_mid = ButBP(
        music,
        freq=(lowfreq+highfreq)/2,
        q=(lowfreq+highfreq)/(2 * (highfreq-lowfreq))  # freq/bandwidth
    )
    music_high = ButHP(music, freq=highfreq)
    fol = 6 * Follower2(
        src,
        risetime=0.1,
        falltime=3,
    )
    music_low.mul = Clip(0.05 + 2 * fol, 0, 0.7)
    music_mid.mul = Clip(0.15 + 0.5 * fol, 0, 0.7)
    music_high.mul = Clip(0.15 + 1 * fol, 0, 0.7)
    return Mix([music_low, music_mid, music_high])


musics = control_music_multi(inx, music).out()

s.gui(locals())
