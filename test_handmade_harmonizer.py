"""kind of like a copy of examples\07-effects\06-hand-made-harmonizer
but simplified"""

from initserver import *

inx = SfPlayer("four-legs-good.wav")
#inx = Input()

respeed = 1.3
window_size = 0.05
ind = Phasor(freq=(1-respeed)/window_size)
win = Pointer(table=WinTable(8), index=ind)  # 8=half-sine
snd = Delay(
    inx,
    delay=ind * window_size,
    #mul=win,  # apparently useful ???
).out()

s.gui(locals())
