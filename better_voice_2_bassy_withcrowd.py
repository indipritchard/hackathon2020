from initserver import *

inx = Input()


def better_voice(src):
    new_dry = Harmonizer(src, transpo=-0.5)  # todo do we want this?
    # new_dry = src
    base_mix = [
        new_dry,
        Harmonizer(new_dry, mul=0.5),
        Disto(new_dry, drive=0.5, mul=0.3),
    ]
    my_centroid = Centroid(new_dry)
    bonus_bass = Harmonizer(
        MoogLP(new_dry, freq=my_centroid * 1.2),
        transpo=-12,
        mul=1,
    )
    verbs = [
        Freeverb(
            Chorus(
                base_mix
            ),
            size=size,
            damp=damp,
            bal=1,
            mul=mul*s_mul,
        )
        for damp, mul in [
            (0.1, 0.1),
            (0.3, 0.4),
            (0.6, 0.5),
            (0.9, 0.1),
        ]
        for size, s_mul in [
            (0.5, 0.5),
            (0.7, 1),
            (0.9, 0.2),
        ]
    ]
    #return bonus_bass
    return Mix([new_dry, bonus_bass] + verbs) 


my_better = better_voice(inx)
my_better.out()


delayed = Delay(inx, delay=2.5, mul=0.5)


def add_crowd_filters(src):
#    my_normals = np.random.randn(0, 1)
    delay_shifts = [
        Delay(src, delay=0.05 * (1 + np.random.randn()), mul=0.1 * np.random.randn() ** 2)
        for i in range(100)
    ]
    pitched = [
        #Disto(
            Harmonizer(d, transpo=3 * np.random.randn())
        #    drive=0.1,
        #)
        for d in delay_shifts
    ]
    #pitched = [
    #    Harmonizer(d, transpo=4) if np.random.randint(0, 2) else d  # minor sixth!... except sounds too high!?
    #    for d in pitched
    #]
    #stdlp = ButLP(src, freq=100)
    #crowdbass = MoogLP(src, freq=100)
    
    outs = pitched
    #outs = [crowdbass]
    #outs = [stdlp]
    return outs


crowds = add_crowd_filters(delayed)

[c.out(1) for c in crowds]  # todo :: how to stereo :(


s.gui(locals())
