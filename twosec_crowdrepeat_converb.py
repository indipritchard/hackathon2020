from initserver import *

#inx = Input()
inx = SfPlayer("four-legs-good.wav")

delayed = Delay(inx, delay=1, maxdelay=2, mul=0.5)


def add_crowd_filters(src, n):
#    my_normals = np.random.randn(0, 1)
    delay_shifts = [
        Delay(src, delay=0.05 * (1 + np.random.randn()), mul=np.random.randn() / n )
        for i in range(n)
    ]
    pitched = [
        #Disto(
            Harmonizer(d, transpo=3 * np.random.randn())
        #    drive=0.1,
        #)
        for d in delay_shifts
    ]
    # pitched = [
    #     Harmonizer(d, transpo=4) if np.random.randint(0, 2) else d  # minor sixth!... except sounds too high!?
    #     for d in pitched
    # ]

    #stdlp = ButLP(src, freq=100)
    #crowdbass = MoogLP(src, freq=100)

    outs = pitched
    #outs = [crowdbass]
    #outs = [stdlp]
    return Mix(outs)


detune_chain = [delayed]
for _ in range(3):
    nxt = add_crowd_filters(detune_chain[-1], 50)
    detune_chain.append(nxt)


# add some of the earlier stages to stop total muddiness!
mix = Mix(detune_chain[2:])
#mix.out()

path = "Battle_Crowd_Celebration.wav"
start_t = 2.3
t1 = SndTable(path, start=start_t, stop=start_t + sampsToSec(512))

convolved = Convolve(
    mix,
    table=t1.mul(HannTable(t1.getSize(False))),
    size=t1.getSize(),
    mul=2)#.mix(2)#.out()

convolved.out()

s.gui(locals())
