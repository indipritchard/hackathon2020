from initserver import *

inx = Input()
#inx = SfPlayer("four-legs-good.wav")

delayed = Delay(inx, delay=2, mul=0.5)


def add_crowd_filters(src):
    delay_shifts = [
        Delay(
            src,
            delay=(
                0.05 * (1 + np.random.randn())
                + Sine(
                    freq=0.05,
                    phase=np.random.random(),
                ).range(0, 0.1)
            ),
            mul=0.02 * np.random.randn() ** 2,
        )
        for i in range(100)
    ]
    pitched = [
        Disto(
            Harmonizer(d, transpo=3 * np.random.randn()),
            drive=0.1,
        )
        for d in delay_shifts
    ]
    return pitched


crowds = add_crowd_filters(delayed)

[c.out() for c in crowds]  # todo :: how to stereo :(

s.gui(locals())
