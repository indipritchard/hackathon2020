from initserver import *

inx = Input()

music = SfPlayer("monstersshort.wav", loop=True).out()


def control_music(src, music):
    fol = Clip(
        6 * Follower2(
            src,
            risetime=0.1,
            falltime=3,
        ),
        0,
        0.5,
    )
    music.mul = 0.1 + fol


control_music(inx, music)

s.gui(locals())
