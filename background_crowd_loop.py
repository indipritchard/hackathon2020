from initserver import *

inx = Input()

# This is an angry crowd which is maybe not what we want?
# Maybe we want a cheering football crowd?
FILEPATH = "angry-crowd.wav"
MUSIC_VOL = 1
music = SfPlayer(FILEPATH, loop=True)

# todo: levels probs not correct

def control_music_multi(src, music, ):
    fol_slow = 12 * Follower2(
        Delay(src, delay=1, maxdelay=2),
        risetime=8,
        falltime=16,
    )

    music.mul = Clip(0.1*fol_slow + 0*2 * (fol_slow ** 2), 0.01, .05)
    return music


musics = Chorus(control_music_multi(inx, music)).out()

def better_voice(src):
    new_dry = Harmonizer(src, transpo=-0.5)  # todo do we want this?
    # new_dry = src
    base_mix = [
        new_dry,
        Harmonizer(new_dry, mul=0.5),
        Disto(new_dry, drive=0.5, mul=0.3),
    ]
    my_centroid = Centroid(new_dry)
    bonus_bass = Harmonizer(
        MoogLP(new_dry, freq=my_centroid * 1.2),
        transpo=-12,
        mul=1,
    )
    verbs = [
        Freeverb(
            Chorus(
                base_mix
            ),
            size=size,
            damp=damp,
            bal=1,
            mul=mul*s_mul,
        )
        for damp, mul in [
            (0.1, 0.1),
            (0.3, 0.4),
            (0.6, 0.5),
            (0.9, 0.1),
        ]
        for size, s_mul in [
            (0.5, 0.5),
            (0.7, 1),
            (0.9, 0.2),
        ]
    ]
    #return bonus_bass
    return Mix([new_dry, bonus_bass] + verbs)


my_better = better_voice(inx)
my_better.out()

s.gui(locals())
