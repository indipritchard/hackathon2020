from initserver import *

inx = Input()

delayed = Delay(inx, delay=2, mul=0.5)


def add_crowd_filters(src):
    delay_shifts = [
        Delay(src, delay=0.001*i, mul=0.1 * np.random.random() ** 3)
        for i in range(400)
    ]
    pitched = [
        Disto(
            Harmonizer(d, transpo=np.random.uniform(-2, 2)),
            drive=0.1,
        )
        for d in delay_shifts
    ]
    #stdlp = ButLP(src, freq=100)
    #crowdbass = MoogLP(src, freq=100)
    
    outs = pitched
    #outs = [crowdbass]
    #outs = [stdlp]
    return outs


crowds = add_crowd_filters(delayed)

[c.out(1) for c in crowds]  # todo :: how to stereo :(

s.gui(locals())
