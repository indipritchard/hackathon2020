from initserver import *


def _load_and_harmonize_music(filepath):
    music_base = SfPlayer(filepath, loop=True)
    return Mix([
        music_base,
        Harmonizer(music_base, transpo=-12),
        Harmonizer(music_base, transpo=-24),
    ])


def control_music(controller, music):
    lowfreq = 130
    highfreq = 400
    music_low = ButLP(music, freq=lowfreq)
    music_mid = ButBP(
        music,
        freq=(lowfreq+highfreq)/2,
        q=(lowfreq+highfreq)/(2 * (highfreq-lowfreq))  # freq/bandwidth
    )
    music_high = ButHP(music, freq=highfreq)
    fol_fast = 6 * Follower2(
        controller,
        risetime=1,
        falltime=2,
    )
    fol_slow = 12 * Follower2(
        Delay(controller, delay=1, maxdelay=2),
        risetime=5,
        falltime=4,
    )
    #music_low.mul = Clip(0.05 + fol_fast + 2.5 * fol_slow, 0, 2)
    #music_mid.mul = Clip(0.05 + fol_fast, 0, 0.7)
    #music_high.mul = Clip(0.05 + fol_slow + fol_fast, 0, 0.8)
    music_low.mul = Clip(fol_slow + 2 * (fol_slow ** 2), 0, 2.75)
    music_mid.mul = Clip(0.4 * fol_fast + 0.8 * (fol_slow + fol_slow ** 2), 0, 1.2)
    music_high.mul = Clip(0.4 * fol_fast + 0.8 * (fol_slow + fol_slow ** 2), 0, 1.2)
    return Mix([music_low, music_mid, music_high])


def control_music_bg(controller, music, delay=1):
    fol_slow = 12 * Follower2(
        Delay(controller, delay=delay, maxdelay=delay),
        risetime=8,
        falltime=16,
    )
    music.mul = Clip(
        0.1 * fol_slow, #+ 0*2 * (fol_slow ** 2),
        0.01, .05
    )
    return music


def get_controlled_music(controller, filename, mul=1):
    controlled = control_music(
        controller=controller,
        music=_load_and_harmonize_music(filename),
    )
    controlled.mul = mul
    return controlled


# nice DRY u idiot
def get_controlled_music_bg(controller, filename, mul=1):
    controlled = control_music_bg(
        controller=controller,
        music=_load_and_harmonize_music(filename),
    )
    controlled.mul = mul
    return controlled


def get_better_voice(
    src,
    base_transpose=-0.5,  # Alden <3 0 ?? Indi <3 -0.5 (but maybe lag)?
):
    if base_transpose:
        new_dry = Harmonizer(src, transpo=base_transpose)
    else:
        new_dry = src
    base_mix = [
        new_dry,
        Harmonizer(new_dry, mul=0.5),
        Disto(new_dry, drive=0.5, mul=0.3),
    ]
    bonus_bass = Harmonizer(
        MoogLP(new_dry, freq=Centroid(new_dry) * 1.2),
        transpo=-12,
        mul=1,
    )
    # todo :: cull some of these verbs for performance?
    # todo :: / less-excessive-reverb-iness
    verbs = [
        Freeverb(
            Chorus(
                base_mix
            ),
            size=size,
            damp=damp,
            bal=1,
            mul=mul*s_mul,
        )
        for damp, mul in [
            (0.1, 0.1),
            (0.3, 0.4),
            (0.6, 0.5),
            (0.9, 0.1),
        ]
        for size, s_mul in [
            (0.5, 0.5),
            (0.7, 1),
            (0.9, 0.2),
        ]
    ]
    return Mix([new_dry, bonus_bass] + verbs)


def add_crowd_filters(src, number):
    delay_shifts = [
        Delay(
            src,
            delay=0.05 * (1 + np.random.randn()),
            mul=np.random.randn() / number,
        )
        for _ in range(number)
    ]
    pitched = [
        # todo :: do we want any distortion?
        #Disto(
            Harmonizer(d, transpo=3 * np.random.randn())
        #    drive=0.1,
        #)
        for d in delay_shifts
    ]
    # todo :: we agreed the following up-pitch sucks, right? :
    # pitched = [
    #     Harmonizer(d, transpo=4) if np.random.randint(0, 2) else d  # minor sixth!... except sounds too high!?
    #     for d in pitched
    # ]
    return Mix(pitched)


def get_convolution_crowd_echo(
    src,
    filename,
    base_delay=2,
    mul=5,
    levels=3,
    people_per_level=50,
):
    detune_chain = [Delay(inx, delay=base_delay, maxdelay=base_delay, mul=3)]
    for _ in range(3):
        detune_chain.append(
            add_crowd_filters(detune_chain[-1], 50)
        )
    start_t = 2.3
    table = SndTable(filename, start=start_t, stop=start_t + sampsToSec(512))
    return Convolve(
        Mix(detune_chain[2:]),  # previous layers to reduce muddiness
        table=table.mul(HannTable(table.getSize(False))),
        size=table.getSize(),
        mul=mul,
    )


# constants
#POWERCHORDS = "looperman/looperman-l-0828980-0200029-sad-slidin-powerchords-g-50.wav"  # music vol 0.5?
#POWERCHORDS_VOL = 0.5
#HARIMENUI = "harimenui_lastbuild.wav"
#HARIMENUI_VOL = 0.5
ORCHESTRAL = "looperman/looperman-l-2986535-0206157-cinematic-strings-staccato-section.wav"  # music vol 1 plz?
ORCHESTRAL_VOL = .4
CROWD_CONVOLVE = "Battle_Crowd_Celebration.wav"
CROWD_BG = "angry-crowd.wav"
CROWD_BG_VOL = .01

# setup inputs and outputs
inx = Input()

input("NO FILTERS ENABLED, next will turn on VOICE")
improved_voice = get_better_voice(inx).out()

input("VOICE ENABLED, next will turn on CROWD ECHO")
crowd = get_convolution_crowd_echo(inx, CROWD_CONVOLVE).out()
bg_crowd = get_controlled_music_bg(inx, CROWD_BG, mul=CROWD_BG_VOL).out()

input("VOICE AND CROWD ENABLED, next will DISABLE and turn on STRINGS")
improved_voice.stop()
crowd.stop()
bg_crowd.stop()
musicz = get_controlled_music(inx, ORCHESTRAL, mul=ORCHESTRAL_VOL).out()

input("STRINGS ENABLED, next will enable ALL FEATURES")
improved_voice.out()
crowd.out()
bg_crowd.out()

input("ALL FEATURES ENABLED, next will KILL EVERYTHING")
improved_voice.stop()
crowd.stop()
bg_crowd.stop()
musicz.stop()

input("ALL FEATURES DISABLED, next will ENABLE EVERYTHING")
improved_voice.out()
crowd.out()
bg_crowd.out()
musicz.out()

input("ALL FEATURES ENABLED, next will QUIT")
s.stop()
#s.gui(locals())
