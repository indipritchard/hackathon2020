from initserver import *

#inx = Input()
inx = SfPlayer("four-legs-good.wav")

delayed = Delay(inx, delay=2, mul=0.5)


def add_crowd_filters(src):
#    my_normals = np.random.randn(0, 1)
    delay_shifts = [
        Delay(src, delay=0.05 * (1 + np.random.randn()), mul=0.02 * np.random.randn() ** 2)
        for i in range(100)
    ]
    pitched = [
        #Disto(
            Harmonizer(d, transpo=3 * np.random.randn())
        #    drive=0.1,
        #)
        for d in delay_shifts
    ]
    #pitched = [
    #    Harmonizer(d, transpo=4) if np.random.randint(0, 2) else d  # minor sixth!... except sounds too high!?
    #    for d in pitched
    #]
    #stdlp = ButLP(src, freq=100)
    #crowdbass = MoogLP(src, freq=100)
    
    outs = pitched
    #outs = [crowdbass]
    #outs = [stdlp]
    return outs


crowds = add_crowd_filters(delayed)

[c.out(1) for c in crowds]  # todo :: how to stereo :(

s.gui(locals())
