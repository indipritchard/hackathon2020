#!/usr/bin/env python
# coding: utf-8

import librosa
import sounddevice as sd
import numpy as np
import soundfile as sf

fname = 'four-legs-good.wav'
# samples, samplerate
s, sr = librosa.load(fname)

# # sd.play(s, sr)
# shifted = librosa.effects.pitch_shift(s, sr, 4)
# sd.play(shifted, sr)

rnd = np.random.randn(100, 7)

offsets = np.array(.1 * sr * rnd[:,0], np.int)

pitchshifts = 4 * rnd[:,1] - 1
#pitchshifts += (rnd[:, 4] > 0)

vols = rnd[:, 2] ** 2

sum = np.zeros_like(s)

for offset, pitchshift, vol in zip(offsets, pitchshifts, vols):
    if offset == 0:
        continue
    shifted = librosa.effects.pitch_shift(s, sr, pitchshift)
    sum[offset:] += shifted[:-offset] * vol

sd.play(sum*.1, sr)

#sf.write("out.wav", sum*.1, sr)
