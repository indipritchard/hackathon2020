from initserver import *

inx = Input()


""" meh x3
FILEPATH = "looperman/looperman-l-0102056-0205857-full-strings-130.wav"  # no bass :(
FILEPATH = "looperman/looperman-l-0173301-0205231-estringsfast.wav"  # a lil bass
FILEPATH = "looperman/looperman-l-2267968-0205766-l-o-v-e-i-s-a-l-o-s-i-n-g-g-a-m-e.wav"  # eh
"""
FILEPATH = "looperman/looperman-l-2986535-0206157-cinematic-strings-staccato-section.wav"  # music vol 1 plz?
#FILEPATH = "looperman/looperman-l-0828980-0200029-sad-slidin-powerchords-g-50.wav"  # music vol 0.5?
#FILEPATH = "harimenui_lastbuild.wav"
MUSIC_VOL = 1
music = SfPlayer(FILEPATH, loop=True)
music2 = Mix([Harmonizer(music, transpo=-24), Harmonizer(music, transpo=-12), music])


def control_music_multi(src, music):
    lowfreq = 130
    highfreq = 400
    music_low = ButLP(music, freq=lowfreq)
    music_mid = ButBP(
        music,
        freq=(lowfreq+highfreq)/2,
        q=(lowfreq+highfreq)/(2 * (highfreq-lowfreq))  # freq/bandwidth
    )
    music_high = ButHP(music, freq=highfreq)
    fol_fast = 6 * Follower2(
        src,
        risetime=0.3,
        falltime=3,
    )
    fol_slow = 12 * Follower2(
        Delay(src, delay=2, maxdelay=2),
        risetime=4,
        falltime=8,
    )
    #music_low.mul = Clip(0.05 + fol_fast + 2.5 * fol_slow, 0, 2)
    #music_mid.mul = Clip(0.05 + fol_fast, 0, 0.7)
    #music_high.mul = Clip(0.05 + fol_slow + fol_fast, 0, 0.8)
    music_low.mul = Clip(fol_slow + 2 * (fol_slow ** 2), 0, 2.5)
    music_mid.mul = Clip(0.4 * fol_fast + 0.8 * (fol_slow + fol_slow ** 2), 0, 1)
    music_high.mul = Clip(0.4 * fol_fast + 0.8 * (fol_slow + fol_slow ** 2), 0, 1)
    return Mix([music_low, music_mid, music_high])


musics = control_music_multi(inx, music2).out()
musics.mul = MUSIC_VOL


def better_voice(src):
    new_dry = Harmonizer(src, transpo=-0.5)  # todo do we want this?
    # new_dry = src
    base_mix = [
        new_dry,
        Harmonizer(new_dry, mul=0.5),
        Disto(new_dry, drive=0.5, mul=0.3),
    ]
    my_centroid = Centroid(new_dry)
    bonus_bass = Harmonizer(
        MoogLP(new_dry, freq=my_centroid * 1.2),
        transpo=-12,
        mul=1,
    )
    verbs = [
        Freeverb(
            Chorus(
                base_mix
            ),
            size=size,
            damp=damp,
            bal=1,
            mul=mul*s_mul,
        )
        for damp, mul in [
            (0.1, 0.1),
            (0.3, 0.4),
            (0.6, 0.5),
            (0.9, 0.1),
        ]
        for size, s_mul in [
            (0.5, 0.5),
            (0.7, 1),
            (0.9, 0.2),
        ]
    ]
    #return bonus_bass
    return Mix([new_dry, bonus_bass] + verbs) 


my_better = better_voice(inx)
my_better.out()


s.gui(locals())
